import pool from '../index';

async function dropUserTable() {
  const connection = await pool.connect();
  await connection.query('DROP TABLE user_table;');
  connection.release();
}

dropUserTable()
  .then(() => {
    console.log('migration passed successfully')})
  .catch((e) => {
    console.log(e, 'migration passed with error')
    });
