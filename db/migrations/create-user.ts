import pool from '../index';

async function createUserTable() {
  const connection = await pool.connect();
  await connection.query('CREATE EXTENSION IF NOT EXISTS pgcrypto;');
  await connection.query(`
    CREATE TABLE IF NOT EXISTS "user_table" (
    uuid text PRIMARY KEY DEFAULT gen_random_uuid(),
    first_name varchar(255),
    last_name varchar(255),
    email varchar(255) UNIQUE NOT NULL);`);

  connection.release();
}

createUserTable()
  .then(() => {
    console.log('migration passed successfully')})
  .catch((e) => {
    console.log(e, 'migration passed with error')
  });
