import pool from "./index";

export interface User {
  uuid?: string
  first_name: string,
  last_name: string,
  email: string
}

export interface Error {
  error?: string
}

export async function addNewUser(first_name: string, last_name: string, email: string): Promise<any>  {
  return await pool.query(`
        insert into "user_table" ("first_name", "last_name", "email")
        values ('${first_name}', '${last_name}', '${email}')`);
}

//TODO: Make fields dynamic
export async function updateUser(uuid: string | undefined, first_name: string, last_name: string, email: string): Promise<any>  {
  return await pool.query(`
        update "user_table" set 
               first_name='${first_name}',
               last_name='${last_name}',
               email='${email}'
        where uuid = '${uuid}'`);
}

export async function removeUser(uuid: string): Promise<any>  {
  return await pool.query(`delete from "user_table" where uuid = '${uuid}'`);
}

export async function getUserList(): Promise<any> {
  return await pool.query(` select uuid, first_name, last_name, email from "user_table" `);
}
