import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import userRouter from './user/router';

const app = new Koa();

app.use(bodyParser());
app.use(userRouter.routes());
app.listen(3000, () => {
  console.log('server started')
})
