# Lecture 13

**Start project**

`.env.example` - for the hw you can use .env.example, just copy and change it
`docker-compose up` - it will automatically run migrations first
`npm run db:remove` - if you need to remove existing db

**Endponits**

`post localhost:3000/user` with body `{"first_name": "Alala 2", "last_name": "Trololo 2", "email": "some@email.com" }` - will create a new user 

`delete localhost:3000/user` with body `{"uuid": "some uuid is here" }` - will remove a new user

`put localhost:3000/user` with body `{ "uuid":"fbb5897d-cf36-4bf9-b5f4-bad14750b2ad", "first_name": "Alala changed", "last_name": "Trololo changed", "email": "some33@email.com" }` - will update existing user

`get localhost:3000/user` - will return all users

