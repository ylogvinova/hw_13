import { Context, Next } from 'koa';
import { getUsers, addUsers, deleteUsers, updateUsers } from './service';
import { User, Error } from '../db/user';

const getStatus = (user: User & Error) => user.error ? 500: 200;
const getBody = (user: User & Error) =>  user.error ? user.error : user;

export async function handleUserGet(ctx: Context, next: Next) {
  const users = await getUsers();
  ctx.body = getBody(users)
  ctx.status = getStatus(users);
  next()
}

export async function handleUserPost(ctx: Context, next: Next) {
  const data = ctx.request.body
  const users = await addUsers(data);
  ctx.body = getBody(users)
  ctx.status = getStatus(users);
  next();
}

export async function handleUserDelete(ctx: Context, next: Next) {
  const data = ctx.request.body
  const users = await deleteUsers(data?.uuid);
  ctx.body = getBody(users)
  ctx.status = getStatus(users);
  next();
}

export async function handleUserUpdate(ctx: Context, next: Next) {
  const data = ctx.request.body
  const users = await updateUsers(data);
  ctx.body = getBody(users)
  ctx.status = getStatus(users);
  next();
}

