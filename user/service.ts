import { User, addNewUser, removeUser, getUserList, updateUser } from '../db/user';

export async function getUsers() {
  try {
    const result = await getUserList();
    return result.rows;
  } catch (err) {
    return { error: err.message }
  } finally {
  }
}

export async function addUsers(data: User) {
  try {
    await addNewUser(data.first_name, data.last_name, data.email);
    const result = await getUserList();
    return result.rows;
  } catch (err) {
    return { error: err.message }
  }
}

export async function updateUsers(data: User) {
  try {
    await updateUser(data.uuid, data.first_name, data.last_name, data.email);
    const result = await getUserList();
    return result.rows;
  } catch (err) {
    return { error: err.message }
  }
}

export async function deleteUsers(uuid: string) {
  try {
    await removeUser(uuid)
    const result = await getUserList();
    return result.rows;
  } catch (err) {
    return { error: err.message }
  }
}
