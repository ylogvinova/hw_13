import Router from 'koa-router';
import { handleUserGet, handleUserDelete, handleUserPost, handleUserUpdate } from './controller';

const router = new Router({ prefix: '/user' });

router.get('/', handleUserGet);
router.delete('/', handleUserDelete);
router.post('/', handleUserPost);
router.put('/', handleUserUpdate);

export default router;
